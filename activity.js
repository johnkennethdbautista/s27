// Users
{
	"id": "user-01",
	"firstName": "John Kenneth",
	"lastName": "Bautista",
	"email": "johnbautista@mail.com",
	"password": "jbautista123",
	"isAdmin": false,
	"mobileNumber": "09123456789"
}

// Orders
{
	"id": "order-001",
	"user_id": "user-01",
	"transactionDate": "04/18/2023 08:40 PM",
	"status": "In-Stock",
	"total": 4
}

// Products
{
	"id": "prod-001",
	"name": "Dell i7",
	"description": "Laptop",
	"price": 27500,
	"stocks": 50,
	"isActive": true,
	"SKU": "cp12396189"
}

// OrderProducts
{
	"id": "OrderProd-001",
	"order_id": "order-001",
	"product_id": "prod-001",
	"quantity": 2,
	"price": 27500,
	"subTotal": 55000
}